package org.savanah.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity(name="Infraction")
@Table(name="\"INFRACTION\"")
public class InfractionEntity extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Size(max = 50)
    @Column(length = 50, name="\"nom\"")
    private String nom;

    @Size(max = 50)
    @Column(length = 50, name="\"peine\"")
    private String peine;

    @ManyToOne(optional=true)
    @JoinColumn(name = "ANTECEDENTJUDICIAIRE_ID", referencedColumnName = "ID")
    private AntecedentJudiciaireEntity antecedentJudiciaire;

    public String getNom() {
        return this.nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPeine() {
        return this.peine;
    }

    public void setPeine(String peine) {
        this.peine = peine;
    }

    public AntecedentJudiciaireEntity getAntecedentJudiciaire() {
        return this.antecedentJudiciaire;
    }

    public void setAntecedentJudiciaire(AntecedentJudiciaireEntity antecedentJudiciaire) {
        this.antecedentJudiciaire = antecedentJudiciaire;
    }

}
