package org.savanah.domain;
import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name="InfosUserAttachment")
public class InfosUserAttachment extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    public InfosUserAttachment() {
        super();
    }
    
    public InfosUserAttachment(Long id, String fileName) {
        this.setId(id);
        this.fileName = fileName;
    }

    @Size(max = 200)
    private String fileName;
    
    @ManyToOne
    @JoinColumn(name = "INFOSUSER_ID", referencedColumnName = "ID")
    private InfosUserEntity infosUser;

    @Lob
    private byte[] content;

    public byte[] getContent() {
        return this.content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }
    
    public String getFileName() {
        return this.fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public InfosUserEntity getInfosUser() {
        return this.infosUser;
    }

    public void setInfosUser(InfosUserEntity infosUser) {
        this.infosUser = infosUser;
    }
}
