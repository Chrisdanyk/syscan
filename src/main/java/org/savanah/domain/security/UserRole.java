package org.savanah.domain.security;

/**
 * User role
 * */
public enum UserRole {

    Administrator, AgentMinJustice, Policier;
}
