package org.savanah.domain;
import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name="CitoyenAttachment")
public class CitoyenAttachment extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    public CitoyenAttachment() {
        super();
    }
    
    public CitoyenAttachment(Long id, String fileName) {
        this.setId(id);
        this.fileName = fileName;
    }

    @Size(max = 200)
    private String fileName;
    
    @ManyToOne
    @JoinColumn(name = "CITOYEN_ID", referencedColumnName = "ID")
    private CitoyenEntity citoyen;

    @Lob
    private byte[] content;

    public byte[] getContent() {
        return this.content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }
    
    public String getFileName() {
        return this.fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public CitoyenEntity getCitoyen() {
        return this.citoyen;
    }

    public void setCitoyen(CitoyenEntity citoyen) {
        this.citoyen = citoyen;
    }
}
