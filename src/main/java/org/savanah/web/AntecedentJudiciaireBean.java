package org.savanah.web;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceException;

import org.primefaces.event.TransferEvent;
import org.primefaces.model.DualListModel;
import org.savanah.domain.AntecedentJudiciaireEntity;
import org.savanah.domain.CitoyenEntity;
import org.savanah.domain.InfractionEntity;
import org.savanah.service.AntecedentJudiciaireService;
import org.savanah.service.CitoyenService;
import org.savanah.service.InfractionService;
import org.savanah.service.security.SecurityWrapper;
import org.savanah.web.generic.GenericLazyDataModel;
import org.savanah.web.util.MessageFactory;

@Named("antecedentJudiciaireBean")
@ViewScoped
public class AntecedentJudiciaireBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger logger = Logger.getLogger(AntecedentJudiciaireBean.class.getName());
    
    private GenericLazyDataModel<AntecedentJudiciaireEntity> lazyModel;
    
    private AntecedentJudiciaireEntity antecedentJudiciaire;
    
    @Inject
    private AntecedentJudiciaireService antecedentJudiciaireService;
    
    @Inject
    private InfractionService infractionService;
    
    @Inject
    private CitoyenService citoyenService;
    
    private DualListModel<InfractionEntity> infractions;
    private List<String> transferedInfractionIDs;
    private List<String> removedInfractionIDs;
    
    private List<CitoyenEntity> availableCitoyenList;
    
    public void prepareNewAntecedentJudiciaire() {
        reset();
        this.antecedentJudiciaire = new AntecedentJudiciaireEntity();
        // set any default values now, if you need
        // Example: this.antecedentJudiciaire.setAnything("test");
    }

    public GenericLazyDataModel<AntecedentJudiciaireEntity> getLazyModel() {
        if (this.lazyModel == null) {
            this.lazyModel = new GenericLazyDataModel<>(antecedentJudiciaireService);
        }
        return this.lazyModel;
    }
    
    public String persist() {

        if (antecedentJudiciaire.getId() == null && !isPermitted("antecedentJudiciaire:create")) {
            return "accessDenied";
        } else if (antecedentJudiciaire.getId() != null && !isPermitted(antecedentJudiciaire, "antecedentJudiciaire:update")) {
            return "accessDenied";
        }
        
        String message;
        
        try {
            
            if (antecedentJudiciaire.getId() != null) {
                antecedentJudiciaire = antecedentJudiciaireService.update(antecedentJudiciaire);
                message = "message_successfully_updated";
            } else {
                antecedentJudiciaire = antecedentJudiciaireService.save(antecedentJudiciaire);
                message = "message_successfully_created";
            }
        } catch (OptimisticLockException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_optimistic_locking_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        } catch (PersistenceException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_save_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }
        
        FacesMessage facesMessage = MessageFactory.getMessage(message);
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        
        return null;
    }
    
    public String delete() {
        
        if (!isPermitted(antecedentJudiciaire, "antecedentJudiciaire:delete")) {
            return "accessDenied";
        }
        
        String message;
        
        try {
            antecedentJudiciaireService.delete(antecedentJudiciaire);
            message = "message_successfully_deleted";
            reset();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_delete_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }
        FacesContext.getCurrentInstance().addMessage(null, MessageFactory.getMessage(message));
        
        return null;
    }
    
    public void onDialogOpen(AntecedentJudiciaireEntity antecedentJudiciaire) {
        reset();
        this.antecedentJudiciaire = antecedentJudiciaire;
    }
    
    public void reset() {
        antecedentJudiciaire = null;

        infractions = null;
        transferedInfractionIDs = null;
        removedInfractionIDs = null;
        
        availableCitoyenList = null;
        
    }

    // Get a List of all citoyen
    public List<CitoyenEntity> getAvailableCitoyen() {
        if (this.availableCitoyenList == null) {
            this.availableCitoyenList = citoyenService.findAvailableCitoyen(this.antecedentJudiciaire);
        }
        return this.availableCitoyenList;
    }
    
    // Update citoyen of the current antecedentJudiciaire
    public void updateCitoyen(CitoyenEntity citoyen) {
        this.antecedentJudiciaire.setCitoyen(citoyen);
        // Maybe we just created and assigned a new citoyen. So reset the availableCitoyenList.
        availableCitoyenList = null;
    }
    
    public DualListModel<InfractionEntity> getInfractions() {
        return infractions;
    }

    public void setInfractions(DualListModel<InfractionEntity> infractions) {
        this.infractions = infractions;
    }
    
    public List<InfractionEntity> getFullInfractionsList() {
        List<InfractionEntity> allList = new ArrayList<>();
        allList.addAll(infractions.getSource());
        allList.addAll(infractions.getTarget());
        return allList;
    }
    
    public void onInfractionsDialog(AntecedentJudiciaireEntity antecedentJudiciaire) {
        // Prepare the infraction PickList
        this.antecedentJudiciaire = antecedentJudiciaire;
        List<InfractionEntity> selectedInfractionsFromDB = infractionService
                .findInfractionsByAntecedentJudiciaire(this.antecedentJudiciaire);
        List<InfractionEntity> availableInfractionsFromDB = infractionService
                .findAvailableInfractions(this.antecedentJudiciaire);
        this.infractions = new DualListModel<>(availableInfractionsFromDB, selectedInfractionsFromDB);
        
        transferedInfractionIDs = new ArrayList<>();
        removedInfractionIDs = new ArrayList<>();
    }
    
    public void onInfractionsPickListTransfer(TransferEvent event) {
        // If a infraction is transferred within the PickList, we just transfer it in this
        // bean scope. We do not change anything it the database, yet.
        for (Object item : event.getItems()) {
            String id = ((InfractionEntity) item).getId().toString();
            if (event.isAdd()) {
                transferedInfractionIDs.add(id);
                removedInfractionIDs.remove(id);
            } else if (event.isRemove()) {
                removedInfractionIDs.add(id);
                transferedInfractionIDs.remove(id);
            }
        }
        
    }
    
    public void updateInfraction(InfractionEntity infraction) {
        // If a new infraction is created, we persist it to the database,
        // but we do not assign it to this antecedentJudiciaire in the database, yet.
        infractions.getTarget().add(infraction);
        transferedInfractionIDs.add(infraction.getId().toString());
    }
    
    public void onInfractionsSubmit() {
        // Now we save the changed of the PickList to the database.
        try {
            List<InfractionEntity> selectedInfractionsFromDB = infractionService
                    .findInfractionsByAntecedentJudiciaire(this.antecedentJudiciaire);
            List<InfractionEntity> availableInfractionsFromDB = infractionService
                    .findAvailableInfractions(this.antecedentJudiciaire);
            
            for (InfractionEntity infraction : selectedInfractionsFromDB) {
                if (removedInfractionIDs.contains(infraction.getId().toString())) {
                    infraction.setAntecedentJudiciaire(null);
                    infractionService.update(infraction);
                }
            }
    
            for (InfractionEntity infraction : availableInfractionsFromDB) {
                if (transferedInfractionIDs.contains(infraction.getId().toString())) {
                    infraction.setAntecedentJudiciaire(antecedentJudiciaire);
                    infractionService.update(infraction);
                }
            }
            
            FacesMessage facesMessage = MessageFactory.getMessage(
                    "message_changes_saved");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
            
            reset();

        } catch (OptimisticLockException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            FacesMessage facesMessage = MessageFactory.getMessage(
                    "message_optimistic_locking_exception");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        } catch (PersistenceException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            FacesMessage facesMessage = MessageFactory.getMessage(
                    "message_picklist_save_exception");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }
    }
    
    public AntecedentJudiciaireEntity getAntecedentJudiciaire() {
        if (this.antecedentJudiciaire == null) {
            prepareNewAntecedentJudiciaire();
        }
        return this.antecedentJudiciaire;
    }
    
    public void setAntecedentJudiciaire(AntecedentJudiciaireEntity antecedentJudiciaire) {
        this.antecedentJudiciaire = antecedentJudiciaire;
    }
    
    public boolean isPermitted(String permission) {
        return SecurityWrapper.isPermitted(permission);
    }

    public boolean isPermitted(AntecedentJudiciaireEntity antecedentJudiciaire, String permission) {
        
        return SecurityWrapper.isPermitted(permission);
        
    }
    
}
