package org.savanah.web;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.activation.MimetypesFileTypeMap;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceException;

import org.apache.commons.io.IOUtils;
import org.imgscalr.Scalr;
import org.imgscalr.Scalr.Method;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.savanah.domain.InfosUserAttachment;
import org.savanah.domain.InfosUserEntity;
import org.savanah.domain.InfosUserImage;
import org.savanah.domain.security.UserEntity;
import org.savanah.service.InfosUserAttachmentService;
import org.savanah.service.InfosUserService;
import org.savanah.service.security.SecurityWrapper;
import org.savanah.service.security.UserService;
import org.savanah.web.generic.GenericLazyDataModel;
import org.savanah.web.util.MessageFactory;

@Named("infosUserBean")
@ViewScoped
public class InfosUserBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger logger = Logger.getLogger(InfosUserBean.class.getName());
    
    private GenericLazyDataModel<InfosUserEntity> lazyModel;
    
    private InfosUserEntity infosUser;
    
    private List<InfosUserAttachment> infosUserAttachments;
    
    @Inject
    private InfosUserService infosUserService;
    
    @Inject
    private InfosUserAttachmentService infosUserAttachmentService;
    
    UploadedFile uploadedImage;
    byte[] uploadedImageContents;
    
    public void prepareNewInfosUser() {
        reset();
        this.infosUser = new InfosUserEntity();
        // set any default values now, if you need
        // Example: this.infosUser.setAnything("test");
    }

    public GenericLazyDataModel<InfosUserEntity> getLazyModel() {
        if (this.lazyModel == null) {
            this.lazyModel = new GenericLazyDataModel<>(infosUserService);
        }
        return this.lazyModel;
    }
    
    public String persist() {

        if (infosUser.getId() == null && !isPermitted("infosUser:create")) {
            return "accessDenied";
        } else if (infosUser.getId() != null && !isPermitted(infosUser, "infosUser:update")) {
            return "accessDenied";
        }
        
        String message;
        
        try {
            
            if (this.uploadedImage != null) {
                try {

                    BufferedImage image;
                    try (InputStream in = new ByteArrayInputStream(uploadedImageContents)) {
                        image = ImageIO.read(in);
                    }
                    image = Scalr.resize(image, Method.BALANCED, 300);

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    ImageOutputStream imageOS = ImageIO.createImageOutputStream(baos);
                    Iterator<ImageWriter> imageWriters = ImageIO.getImageWritersByMIMEType(
                            uploadedImage.getContentType());
                    ImageWriter imageWriter = (ImageWriter) imageWriters.next();
                    imageWriter.setOutput(imageOS);
                    imageWriter.write(image);
                    
                    baos.close();
                    imageOS.close();
                    
                    logger.log(Level.INFO, "Resized uploaded image from {0} to {1}", new Object[]{uploadedImageContents.length, baos.toByteArray().length});
            
                    InfosUserImage infosUserImage = new InfosUserImage();
                    infosUserImage.setContent(baos.toByteArray());
                    infosUser.setImage(infosUserImage);
                } catch (Exception e) {
                    FacesMessage facesMessage = MessageFactory.getMessage(
                            "message_upload_exception");
                    FacesContext.getCurrentInstance().addMessage(null, facesMessage);
                    FacesContext.getCurrentInstance().validationFailed();
                    return null;
                }
            }
            
            if (infosUser.getId() != null) {
                infosUser = infosUserService.update(infosUser);
                message = "message_successfully_updated";
            } else {
                infosUser = infosUserService.save(infosUser);
                message = "message_successfully_created";
            }
        } catch (OptimisticLockException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_optimistic_locking_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        } catch (PersistenceException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_save_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }
        
        FacesMessage facesMessage = MessageFactory.getMessage(message);
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        
        return null;
    }
    
    public String delete() {
        
        if (!isPermitted(infosUser, "infosUser:delete")) {
            return "accessDenied";
        }
        
        String message;
        
        try {
            infosUserService.delete(infosUser);
            message = "message_successfully_deleted";
            reset();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_delete_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }
        FacesContext.getCurrentInstance().addMessage(null, MessageFactory.getMessage(message));
        
        return null;
    }
    
    public void onDialogOpen(InfosUserEntity infosUser) {
        reset();
        this.infosUser = infosUser;
    }
    
    public void reset() {
        infosUser = null;

        infosUserAttachments = null;
        
        uploadedImage = null;
        uploadedImageContents = null;
        
    }

    public void handleImageUpload(FileUploadEvent event) {
        
        Iterator<ImageWriter> imageWriters = ImageIO.getImageWritersByMIMEType(
                event.getFile().getContentType());
        if (!imageWriters.hasNext()) {
            FacesMessage facesMessage = MessageFactory.getMessage(
                    "message_image_type_not_supported");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
            return;
        }
        
        this.uploadedImage = event.getFile();
        this.uploadedImageContents = event.getFile().getContents();
        
        FacesMessage message = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
    
    public byte[] getUploadedImageContents() {
        if (uploadedImageContents != null) {
            return uploadedImageContents;
        } else if (infosUser != null && infosUser.getImage() != null) {
            infosUser = infosUserService.lazilyLoadImageToInfosUser(infosUser);
            return infosUser.getImage().getContent();
        }
        return null;
    }
    
    public List<InfosUserAttachment> getInfosUserAttachments() {
        if (this.infosUserAttachments == null && this.infosUser != null && this.infosUser.getId() != null) {
            // The byte streams are not loaded from database with following line. This would cost too much.
            this.infosUserAttachments = this.infosUserAttachmentService.getAttachmentsList(infosUser);
        }
        return this.infosUserAttachments;
    }
    
    public void handleAttachmentUpload(FileUploadEvent event) {
        
        InfosUserAttachment infosUserAttachment = new InfosUserAttachment();
        
        try {
            // Would be better to use ...getFile().getContents(), but does not work on every environment
            infosUserAttachment.setContent(IOUtils.toByteArray(event.getFile().getInputstream()));
        
            infosUserAttachment.setFileName(event.getFile().getFileName());
            infosUserAttachment.setInfosUser(infosUser);
            infosUserAttachmentService.save(infosUserAttachment);
            
            // set infosUserAttachment to null, will be refreshed on next demand
            this.infosUserAttachments = null;
            
            FacesMessage facesMessage = MessageFactory.getMessage(
                    "message_successfully_uploaded");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
            
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            FacesMessage facesMessage = MessageFactory.getMessage(
                    "message_upload_exception");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }
    }

    public StreamedContent getAttachmentStreamedContent(
            InfosUserAttachment infosUserAttachment) {
        if (infosUserAttachment != null && infosUserAttachment.getContent() == null) {
            infosUserAttachment = infosUserAttachmentService
                    .find(infosUserAttachment.getId());
        }
        return new DefaultStreamedContent(new ByteArrayInputStream(
                infosUserAttachment.getContent()),
                new MimetypesFileTypeMap().getContentType(infosUserAttachment
                        .getFileName()), infosUserAttachment.getFileName());
    }

    public String deleteAttachment(InfosUserAttachment attachment) {
        
        infosUserAttachmentService.delete(attachment);
        
        // set infosUserAttachment to null, will be refreshed on next demand
        this.infosUserAttachments = null;
        
        FacesMessage facesMessage = MessageFactory.getMessage(
                "message_successfully_deleted", "Attachment");
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        return null;
    }
    
    public InfosUserEntity getInfosUser() {
        if (this.infosUser == null) {
            prepareNewInfosUser();
        }
        return this.infosUser;
    }
    
    public void setInfosUser(InfosUserEntity infosUser) {
        this.infosUser = infosUser;
    }
    
    public boolean isPermitted(String permission) {
        return SecurityWrapper.isPermitted(permission);
    }

    public boolean isPermitted(InfosUserEntity infosUser, String permission) {
        
        return SecurityWrapper.isPermitted(permission);
        
    }
    
}
