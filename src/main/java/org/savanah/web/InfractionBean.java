package org.savanah.web;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceException;
import org.primefaces.event.FlowEvent;
import org.savanah.domain.AntecedentJudiciaireEntity;
import org.savanah.domain.CitoyenEntity;

import org.savanah.domain.InfractionEntity;
import org.savanah.service.AntecedentJudiciaireService;
import org.savanah.service.CitoyenService;
import org.savanah.service.InfractionService;
import org.savanah.service.security.SecurityWrapper;
import org.savanah.web.generic.GenericLazyDataModel;
import org.savanah.web.util.MessageFactory;

@Named("infractionBean")
@ViewScoped
public class InfractionBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger logger = Logger.getLogger(InfractionBean.class.getName());

    private GenericLazyDataModel<InfractionEntity> lazyModel;

    private InfractionEntity infraction;
    private CitoyenEntity citoyen;
    private AntecedentJudiciaireEntity antecedent;

    @Inject
    private InfractionService infractionService;
    @Inject
    private CitoyenService citoyenService;
    @Inject
    private AntecedentJudiciaireService antecedentJudiciaireService;
    private boolean skip;

    public void prepareNewInfraction() {
        reset();
        this.infraction = new InfractionEntity();
        // set any default values now, if you need
        // Example: this.infraction.setAnything("test");
    }

    public GenericLazyDataModel<InfractionEntity> getLazyModel() {
        if (this.lazyModel == null) {
            this.lazyModel = new GenericLazyDataModel<>(infractionService);
        }
        return this.lazyModel;
    }

    public String persist() {
        if (infraction.getId() == null && !isPermitted("infraction:create")) {
            return "accessDenied";
        } else if (infraction.getId() != null && !isPermitted(infraction, "infraction:update")) {
            return "accessDenied";
        }

        String message;
        try {
            if (infraction.getId() != null) {
                infraction = infractionService.update(infraction);
                message = "message_successfully_updated";
            } else {
                CitoyenEntity citoyenEntity = new CitoyenEntity();
//                citoyenEntity.setAdresse("adresse");
//                citoyenEntity.setNom("xis xis");
                AntecedentJudiciaireEntity antecedentJudiciaireEntity = new AntecedentJudiciaireEntity();
//                antecedentJudiciaireEntity.setDate(new Date());
//                antecedentJudiciaireEntity.setDescription("hakakkajjk");
                antecedentJudiciaireEntity.setCitoyen(citoyenEntity);
                infraction.setAntecedentJudiciaire(antecedentJudiciaireEntity);
                infraction = infractionService.save(infraction);
                message = "message_successfully_created";
            }
        } catch (OptimisticLockException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_optimistic_locking_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        } catch (PersistenceException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_save_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }

        FacesMessage facesMessage = MessageFactory.getMessage(message);
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);

        return null;
    }

    public String delete() {

        if (!isPermitted(infraction, "infraction:delete")) {
            return "accessDenied";
        }

        String message;

        try {
            infractionService.delete(infraction);
            message = "message_successfully_deleted";
            reset();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_delete_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }
        FacesContext.getCurrentInstance().addMessage(null, MessageFactory.getMessage(message));

        return null;
    }

    public void onDialogOpen(InfractionEntity infraction) {
        reset();
        this.infraction = infraction;
    }

    public void reset() {
        infraction = null;

    }

    public InfractionEntity getInfraction() {
        if (this.infraction == null) {
            prepareNewInfraction();
        }
        return this.infraction;
    }

    public void setInfraction(InfractionEntity infraction) {
        this.infraction = infraction;
    }

    public boolean isPermitted(String permission) {
        return SecurityWrapper.isPermitted(permission);
    }

    public boolean isPermitted(InfractionEntity infraction, String permission) {

        return SecurityWrapper.isPermitted(permission);

    }

    public List<InfractionEntity> getAllInfraction() {

        return infractionService.findAllInfractionEntities();
    }

    public boolean isSkip() {
        return skip;
    }

    public void setSkip(boolean skip) {
        this.skip = skip;
    }

    public String onFlowProcess(FlowEvent event) {
        if (skip) {
            skip = false;   //reset in case user goes back
            return "confirm";
        } else {
            return event.getNewStep();
        }
    }

    public CitoyenEntity getCitoyen() {
        if (this.citoyen == null) {
            reset();
            this.citoyen = new CitoyenEntity();
        }
        return this.citoyen;
    }

    public AntecedentJudiciaireEntity getAntecedent() {
        if (this.antecedent == null) {
            reset();
            this.antecedent = new AntecedentJudiciaireEntity();
        }
        return this.antecedent;
    }

    public void setCitoyen(CitoyenEntity citoyen) {
        this.citoyen = citoyen;
    }

    public void setAntecedent(AntecedentJudiciaireEntity antecedent) {
        this.antecedent = antecedent;
    }

}
