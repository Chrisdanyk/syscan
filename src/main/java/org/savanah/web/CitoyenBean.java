package org.savanah.web;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.activation.MimetypesFileTypeMap;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceException;

import org.apache.commons.io.IOUtils;
import org.imgscalr.Scalr;
import org.imgscalr.Scalr.Method;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.FlowEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.savanah.domain.CitoyenAttachment;
import org.savanah.domain.CitoyenEntity;
import org.savanah.domain.CitoyenImage;
import org.savanah.service.CitoyenAttachmentService;
import org.savanah.service.CitoyenService;
import org.savanah.service.security.SecurityWrapper;
import org.savanah.web.generic.GenericLazyDataModel;
import org.savanah.web.util.MessageFactory;

@Named("citoyenBean")
@ViewScoped
public class CitoyenBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger logger = Logger.getLogger(CitoyenBean.class.getName());

    private GenericLazyDataModel<CitoyenEntity> lazyModel;

    private CitoyenEntity citoyen;

    private List<CitoyenAttachment> citoyenAttachments;

    @Inject
    private CitoyenService citoyenService;

    @Inject
    private CitoyenAttachmentService citoyenAttachmentService;
    private boolean skip;
    UploadedFile uploadedImage;
    byte[] uploadedImageContents;

    public void prepareNewCitoyen() {
        reset();
        this.citoyen = new CitoyenEntity();
        // set any default values now, if you need
        // Example: this.citoyen.setAnything("test");
    }

    public GenericLazyDataModel<CitoyenEntity> getLazyModel() {
        if (this.lazyModel == null) {
            this.lazyModel = new GenericLazyDataModel<>(citoyenService);
        }
        return this.lazyModel;
    }

    public String persist() {

        if (citoyen.getId() == null && !isPermitted("citoyen:create")) {
            return "accessDenied";
        } else if (citoyen.getId() != null && !isPermitted(citoyen, "citoyen:update")) {
            return "accessDenied";
        }

        String message;

        try {

            if (this.uploadedImage != null) {
                try {

                    BufferedImage image;
                    try (InputStream in = new ByteArrayInputStream(uploadedImageContents)) {
                        image = ImageIO.read(in);
                    }
                    image = Scalr.resize(image, Method.BALANCED, 300);

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    ImageOutputStream imageOS = ImageIO.createImageOutputStream(baos);
                    Iterator<ImageWriter> imageWriters = ImageIO.getImageWritersByMIMEType(
                            uploadedImage.getContentType());
                    ImageWriter imageWriter = (ImageWriter) imageWriters.next();
                    imageWriter.setOutput(imageOS);
                    imageWriter.write(image);

                    baos.close();
                    imageOS.close();

                    logger.log(Level.INFO, "Resized uploaded image from {0} to {1}", new Object[]{uploadedImageContents.length, baos.toByteArray().length});

                    CitoyenImage citoyenImage = new CitoyenImage();
                    citoyenImage.setContent(baos.toByteArray());
                    citoyen.setImage(citoyenImage);
                } catch (Exception e) {
                    FacesMessage facesMessage = MessageFactory.getMessage(
                            "message_upload_exception");
                    FacesContext.getCurrentInstance().addMessage(null, facesMessage);
                    FacesContext.getCurrentInstance().validationFailed();
                    return null;
                }
            }

            if (citoyen.getId() != null) {
                citoyen = citoyenService.update(citoyen);
                message = "message_successfully_updated";
            } else {
                citoyen = citoyenService.save(citoyen);
                message = "message_successfully_created";
            }
        } catch (OptimisticLockException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_optimistic_locking_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        } catch (PersistenceException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_save_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }

        FacesMessage facesMessage = MessageFactory.getMessage(message);
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);

        return null;
    }

    public String delete() {

        if (!isPermitted(citoyen, "citoyen:delete")) {
            return "accessDenied";
        }

        String message;

        try {
            citoyenService.delete(citoyen);
            message = "message_successfully_deleted";
            reset();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_delete_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }
        FacesContext.getCurrentInstance().addMessage(null, MessageFactory.getMessage(message));

        return null;
    }

    public void onDialogOpen(CitoyenEntity citoyen) {
        reset();
        this.citoyen = citoyen;
    }

    public void reset() {
        citoyen = null;

        citoyenAttachments = null;

        uploadedImage = null;
        uploadedImageContents = null;

    }

    public void handleImageUpload(FileUploadEvent event) {

        Iterator<ImageWriter> imageWriters = ImageIO.getImageWritersByMIMEType(
                event.getFile().getContentType());
        if (!imageWriters.hasNext()) {
            FacesMessage facesMessage = MessageFactory.getMessage(
                    "message_image_type_not_supported");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
            return;
        }

        this.uploadedImage = event.getFile();
        this.uploadedImageContents = event.getFile().getContents();

        FacesMessage message = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public byte[] getUploadedImageContents() {
        if (uploadedImageContents != null) {
            return uploadedImageContents;
        } else if (citoyen != null && citoyen.getImage() != null) {
            citoyen = citoyenService.lazilyLoadImageToCitoyen(citoyen);
            return citoyen.getImage().getContent();
        }
        return null;
    }

    public List<CitoyenAttachment> getCitoyenAttachments() {
        if (this.citoyenAttachments == null && this.citoyen != null && this.citoyen.getId() != null) {
            // The byte streams are not loaded from database with following line. This would cost too much.
            this.citoyenAttachments = this.citoyenAttachmentService.getAttachmentsList(citoyen);
        }
        return this.citoyenAttachments;
    }

    public void handleAttachmentUpload(FileUploadEvent event) {

        CitoyenAttachment citoyenAttachment = new CitoyenAttachment();

        try {
            // Would be better to use ...getFile().getContents(), but does not work on every environment
            citoyenAttachment.setContent(IOUtils.toByteArray(event.getFile().getInputstream()));

            citoyenAttachment.setFileName(event.getFile().getFileName());
            citoyenAttachment.setCitoyen(citoyen);
            citoyenAttachmentService.save(citoyenAttachment);

            // set citoyenAttachment to null, will be refreshed on next demand
            this.citoyenAttachments = null;

            FacesMessage facesMessage = MessageFactory.getMessage(
                    "message_successfully_uploaded");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);

        } catch (IOException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            FacesMessage facesMessage = MessageFactory.getMessage(
                    "message_upload_exception");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }
    }

    public StreamedContent getAttachmentStreamedContent(
            CitoyenAttachment citoyenAttachment) {
        if (citoyenAttachment != null && citoyenAttachment.getContent() == null) {
            citoyenAttachment = citoyenAttachmentService
                    .find(citoyenAttachment.getId());
        }
        return new DefaultStreamedContent(new ByteArrayInputStream(
                citoyenAttachment.getContent()),
                new MimetypesFileTypeMap().getContentType(citoyenAttachment
                        .getFileName()), citoyenAttachment.getFileName());
    }

    public String deleteAttachment(CitoyenAttachment attachment) {

        citoyenAttachmentService.delete(attachment);

        // set citoyenAttachment to null, will be refreshed on next demand
        this.citoyenAttachments = null;

        FacesMessage facesMessage = MessageFactory.getMessage(
                "message_successfully_deleted", "Attachment");
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        return null;
    }

    public CitoyenEntity getCitoyen() {
        if (this.citoyen == null) {
            prepareNewCitoyen();
        }
        return this.citoyen;
    }

    public void setCitoyen(CitoyenEntity citoyen) {
        this.citoyen = citoyen;
    }

    public boolean isPermitted(String permission) {
        return SecurityWrapper.isPermitted(permission);
    }

    public boolean isPermitted(CitoyenEntity citoyen, String permission) {

        return SecurityWrapper.isPermitted(permission);

    }

    public boolean isSkip() {
        return skip;
    }

    public void setSkip(boolean skip) {
        this.skip = skip;
    }

    public String onFlowProcess(FlowEvent event) {
        if (skip) {
            skip = false;   //reset in case user goes back
            return "confirm";
        } else {
            return event.getNewStep();
        }
    }
}
