package org.savanah.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;
import javax.transaction.Transactional;

import org.savanah.domain.InfosUserAttachment;
import org.savanah.domain.InfosUserEntity;

@Named
public class InfosUserAttachmentService extends BaseService<InfosUserAttachment> implements Serializable {

    private static final long serialVersionUID = 1L;
    
    public InfosUserAttachmentService(){
        super(InfosUserAttachment.class);
    }
    
    @Override
    @Transactional
    public long countAllEntries() {
        return entityManager.createQuery("SELECT COUNT(o) FROM InfosUserAttachment o", Long.class).getSingleResult();
    }

    @Transactional
    public void deleteAttachmentsByInfosUser(InfosUserEntity infosUser) {
        entityManager
                .createQuery("DELETE FROM InfosUserAttachment c WHERE c.infosUser = :p")
                .setParameter("p", infosUser).executeUpdate();
    }
    
    @Transactional
    public List<InfosUserAttachment> getAttachmentsList(InfosUserEntity infosUser) {
        if (infosUser == null || infosUser.getId() == null) {
            return new ArrayList<>();
        }
        // The byte streams are not loaded from database with following line. This would cost too much.
        return entityManager.createQuery("SELECT NEW org.savanah.domain.InfosUserAttachment(o.id, o.fileName) FROM InfosUserAttachment o WHERE o.infosUser.id = :id", InfosUserAttachment.class).setParameter("id", infosUser.getId()).getResultList();
    }
}
