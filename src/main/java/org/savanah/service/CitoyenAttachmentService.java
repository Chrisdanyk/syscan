package org.savanah.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;
import javax.transaction.Transactional;

import org.savanah.domain.CitoyenAttachment;
import org.savanah.domain.CitoyenEntity;

@Named
public class CitoyenAttachmentService extends BaseService<CitoyenAttachment> implements Serializable {

    private static final long serialVersionUID = 1L;
    
    public CitoyenAttachmentService(){
        super(CitoyenAttachment.class);
    }
    
    @Override
    @Transactional
    public long countAllEntries() {
        return entityManager.createQuery("SELECT COUNT(o) FROM CitoyenAttachment o", Long.class).getSingleResult();
    }

    @Transactional
    public void deleteAttachmentsByCitoyen(CitoyenEntity citoyen) {
        entityManager
                .createQuery("DELETE FROM CitoyenAttachment c WHERE c.citoyen = :p")
                .setParameter("p", citoyen).executeUpdate();
    }
    
    @Transactional
    public List<CitoyenAttachment> getAttachmentsList(CitoyenEntity citoyen) {
        if (citoyen == null || citoyen.getId() == null) {
            return new ArrayList<>();
        }
        // The byte streams are not loaded from database with following line. This would cost too much.
        return entityManager.createQuery("SELECT NEW org.savanah.domain.CitoyenAttachment(o.id, o.fileName) FROM CitoyenAttachment o WHERE o.citoyen.id = :id", CitoyenAttachment.class).setParameter("id", citoyen.getId()).getResultList();
    }
}
