package org.savanah.service;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.PersistenceUnitUtil;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.primefaces.model.SortOrder;
import org.savanah.domain.InfosUserEntity;
import org.savanah.domain.security.UserEntity;
import org.savanah.service.security.UserService;

@Named
public class InfosUserService extends BaseService<InfosUserEntity> implements Serializable {

    private static final long serialVersionUID = 1L;
    
    public InfosUserService(){
        super(InfosUserEntity.class);
    }
    
    @Inject
    private InfosUserAttachmentService attachmentService;
    
    @Transactional
    public List<InfosUserEntity> findAllInfosUserEntities() {
        
        return entityManager.createQuery("SELECT o FROM InfosUser o ", InfosUserEntity.class).getResultList();
    }
    
    @Override
    @Transactional
    public long countAllEntries() {
        return entityManager.createQuery("SELECT COUNT(o) FROM InfosUser o", Long.class).getSingleResult();
    }
    
    @Override
    protected void handleDependenciesBeforeDelete(InfosUserEntity infosUser) {

        /* This is called before a InfosUser is deleted. Place here all the
           steps to cut dependencies to other entities */
        
        this.attachmentService.deleteAttachmentsByInfosUser(infosUser);
        
        this.cutAllInfosUserUsersAssignments(infosUser);
        
    }

    // Remove all assignments from all user a infosUser. Called before delete a infosUser.
    @Transactional
    private void cutAllInfosUserUsersAssignments(InfosUserEntity infosUser) {
        entityManager
                .createQuery("UPDATE User c SET c.infosUser = NULL WHERE c.infosUser = :p")
                .setParameter("p", infosUser).executeUpdate();
    }
    
    // Find all user which are not yet assigned to a infosUser
    @Transactional
    public List<InfosUserEntity> findAvailableInfosUser(UserEntity user) {
        Long id = -1L;
        if (user != null && user.getInfosUser() != null && user.getInfosUser().getId() != null) {
            id = user.getInfosUser().getId();
        }
        return entityManager.createQuery(
                "SELECT o FROM InfosUser o where o.id NOT IN (SELECT p.infosUser.id FROM User p where p.infosUser.id != :id)", InfosUserEntity.class)
                .setParameter("id", id).getResultList();    
    }

    @Transactional
    public InfosUserEntity lazilyLoadImageToInfosUser(InfosUserEntity infosUser) {
        PersistenceUnitUtil u = entityManager.getEntityManagerFactory().getPersistenceUnitUtil();
        if (!u.isLoaded(infosUser, "image") && infosUser.getId() != null) {
            infosUser = find(infosUser.getId());
            infosUser.getImage().getId();
        }
        return infosUser;
    }
    
    // This is the central method called by the DataTable
    @Override
    @Transactional
    public List<InfosUserEntity> findEntriesPagedAndFilteredAndSorted(int firstResult, int maxResults, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        
        StringBuilder query = new StringBuilder();

        query.append("SELECT o FROM InfosUser o");
        
        query.append(" LEFT JOIN FETCH o.image");
        
        String nextConnective = " WHERE";
        
        Map<String, Object> queryParameters = new HashMap<>();
        
        if (filters != null && !filters.isEmpty()) {
            
            nextConnective += " ( ";
            
            for(String filterProperty : filters.keySet()) {
                
                if (filters.get(filterProperty) == null) {
                    continue;
                }
                
                switch (filterProperty) {
                
                case "nom":
                    query.append(nextConnective).append(" o.nom LIKE :nom");
                    queryParameters.put("nom", "%" + filters.get(filterProperty) + "%");
                    break;

                case "postNom":
                    query.append(nextConnective).append(" o.postNom LIKE :postNom");
                    queryParameters.put("postNom", "%" + filters.get(filterProperty) + "%");
                    break;

                case "telephone":
                    query.append(nextConnective).append(" o.telephone LIKE :telephone");
                    queryParameters.put("telephone", "%" + filters.get(filterProperty) + "%");
                    break;

                case "numIdentification":
                    query.append(nextConnective).append(" o.numIdentification LIKE :numIdentification");
                    queryParameters.put("numIdentification", "%" + filters.get(filterProperty) + "%");
                    break;

                case "adresse":
                    query.append(nextConnective).append(" o.adresse LIKE :adresse");
                    queryParameters.put("adresse", "%" + filters.get(filterProperty) + "%");
                    break;

                }
                
                nextConnective = " AND";
            }
            
            query.append(" ) ");
            nextConnective = " AND";
        }
        
        if (sortField != null && !sortField.isEmpty()) {
            query.append(" ORDER BY o.").append(sortField);
            query.append(SortOrder.DESCENDING.equals(sortOrder) ? " DESC" : " ASC");
        }
        
        TypedQuery<InfosUserEntity> q = this.entityManager.createQuery(query.toString(), this.getType());
        
        for(String queryParameter : queryParameters.keySet()) {
            q.setParameter(queryParameter, queryParameters.get(queryParameter));
        }

        return q.setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
}
