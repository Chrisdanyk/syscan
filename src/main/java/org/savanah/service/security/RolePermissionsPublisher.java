package org.savanah.service.security;

import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import org.savanah.domain.security.RolePermission;
import org.savanah.domain.security.UserRole;

@Singleton
@Startup
public class RolePermissionsPublisher {

    private static final Logger logger = Logger.getLogger(RolePermissionsPublisher.class.getName());
    
    @Inject
    private RolePermissionsService rolePermissionService;
    
    @PostConstruct
    public void postConstruct() {

        if (rolePermissionService.countAllEntries() == 0) {

            rolePermissionService.save(new RolePermission(UserRole.Administrator, "citoyen:create"));
            
            rolePermissionService.save(new RolePermission(UserRole.Administrator, "citoyen:read"));
            
            rolePermissionService.save(new RolePermission(UserRole.Administrator, "citoyen:update"));
            
            rolePermissionService.save(new RolePermission(UserRole.Administrator, "citoyen:delete"));
            
            rolePermissionService.save(new RolePermission(UserRole.AgentMinJustice, "citoyen:create"));
            
            rolePermissionService.save(new RolePermission(UserRole.AgentMinJustice, "citoyen:read"));
            
            rolePermissionService.save(new RolePermission(UserRole.AgentMinJustice, "citoyen:update"));
            
            rolePermissionService.save(new RolePermission(UserRole.Policier, "citoyen:create"));
            
            rolePermissionService.save(new RolePermission(UserRole.Administrator, "infraction:create"));
            
            rolePermissionService.save(new RolePermission(UserRole.Administrator, "infraction:read"));
            
            rolePermissionService.save(new RolePermission(UserRole.Administrator, "infraction:update"));
            
            rolePermissionService.save(new RolePermission(UserRole.Administrator, "infraction:delete"));
            
            rolePermissionService.save(new RolePermission(UserRole.AgentMinJustice, "infraction:create"));
            
            rolePermissionService.save(new RolePermission(UserRole.AgentMinJustice, "infraction:read"));
            
            rolePermissionService.save(new RolePermission(UserRole.AgentMinJustice, "infraction:update"));
            
            rolePermissionService.save(new RolePermission(UserRole.Policier, "infraction:read"));
            
            rolePermissionService.save(new RolePermission(UserRole.Administrator, "antecedentJudiciaire:create"));
            
            rolePermissionService.save(new RolePermission(UserRole.Administrator, "antecedentJudiciaire:read"));
            
            rolePermissionService.save(new RolePermission(UserRole.Administrator, "antecedentJudiciaire:update"));
            
            rolePermissionService.save(new RolePermission(UserRole.Administrator, "antecedentJudiciaire:delete"));
            
            rolePermissionService.save(new RolePermission(UserRole.AgentMinJustice, "antecedentJudiciaire:create"));
            
            rolePermissionService.save(new RolePermission(UserRole.AgentMinJustice, "antecedentJudiciaire:read"));
            
            rolePermissionService.save(new RolePermission(UserRole.AgentMinJustice, "antecedentJudiciaire:update"));
            
            rolePermissionService.save(new RolePermission(UserRole.Policier, "antecedentJudiciaire:read"));
            
            rolePermissionService.save(new RolePermission(UserRole.Administrator, "infosUser:create"));
            
            rolePermissionService.save(new RolePermission(UserRole.Administrator, "infosUser:read"));
            
            rolePermissionService.save(new RolePermission(UserRole.Administrator, "infosUser:update"));
            
            rolePermissionService.save(new RolePermission(UserRole.Administrator, "infosUser:delete"));
            
            rolePermissionService.save(new RolePermission(UserRole.AgentMinJustice, "infosUser:create"));
            
            rolePermissionService.save(new RolePermission(UserRole.AgentMinJustice, "infosUser:read"));
            
            rolePermissionService.save(new RolePermission(UserRole.AgentMinJustice, "infosUser:update"));
            
            rolePermissionService.save(new RolePermission(UserRole.Policier, "infosUser:read"));
            
            rolePermissionService.save(new RolePermission(UserRole.Administrator, "user:*"));
            
            logger.info("Successfully created permissions for user roles.");
        }
    }
}
