package org.savanah.service;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.PersistenceUnitUtil;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.primefaces.model.SortOrder;
import org.savanah.domain.AntecedentJudiciaireEntity;
import org.savanah.domain.CitoyenEntity;

@Named
public class CitoyenService extends BaseService<CitoyenEntity> implements Serializable {

    private static final long serialVersionUID = 1L;
    
    public CitoyenService(){
        super(CitoyenEntity.class);
    }
    
    @Inject
    private CitoyenAttachmentService attachmentService;
    
    @Transactional
    public List<CitoyenEntity> findAllCitoyenEntities() {
        
        return entityManager.createQuery("SELECT o FROM Citoyen o ", CitoyenEntity.class).getResultList();
    }
    
    @Override
    @Transactional
    public long countAllEntries() {
        return entityManager.createQuery("SELECT COUNT(o) FROM Citoyen o", Long.class).getSingleResult();
    }
    
    @Override
    protected void handleDependenciesBeforeDelete(CitoyenEntity citoyen) {

        /* This is called before a Citoyen is deleted. Place here all the
           steps to cut dependencies to other entities */
        
        this.attachmentService.deleteAttachmentsByCitoyen(citoyen);
        
        this.cutAllCitoyenAntecedentJudiciairesAssignments(citoyen);
        
    }

    // Remove all assignments from all antecedentJudiciaire a citoyen. Called before delete a citoyen.
    @Transactional
    private void cutAllCitoyenAntecedentJudiciairesAssignments(CitoyenEntity citoyen) {
        entityManager
                .createQuery("UPDATE AntecedentJudiciaire c SET c.citoyen = NULL WHERE c.citoyen = :p")
                .setParameter("p", citoyen).executeUpdate();
    }
    
    // Find all antecedentJudiciaire which are not yet assigned to a citoyen
    @Transactional
    public List<CitoyenEntity> findAvailableCitoyen(AntecedentJudiciaireEntity antecedentJudiciaire) {
        Long id = -1L;
        if (antecedentJudiciaire != null && antecedentJudiciaire.getCitoyen() != null && antecedentJudiciaire.getCitoyen().getId() != null) {
            id = antecedentJudiciaire.getCitoyen().getId();
        }
        return entityManager.createQuery(
                "SELECT o FROM Citoyen o where o.id NOT IN (SELECT p.citoyen.id FROM AntecedentJudiciaire p where p.citoyen.id != :id)", CitoyenEntity.class)
                .setParameter("id", id).getResultList();    
    }

    @Transactional
    public CitoyenEntity lazilyLoadImageToCitoyen(CitoyenEntity citoyen) {
        PersistenceUnitUtil u = entityManager.getEntityManagerFactory().getPersistenceUnitUtil();
        if (!u.isLoaded(citoyen, "image") && citoyen.getId() != null) {
            citoyen = find(citoyen.getId());
            citoyen.getImage().getId();
        }
        return citoyen;
    }
    
    // This is the central method called by the DataTable
    @Override
    @Transactional
    public List<CitoyenEntity> findEntriesPagedAndFilteredAndSorted(int firstResult, int maxResults, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        
        StringBuilder query = new StringBuilder();

        query.append("SELECT o FROM Citoyen o");
        
        query.append(" LEFT JOIN FETCH o.image");
        
        String nextConnective = " WHERE";
        
        Map<String, Object> queryParameters = new HashMap<>();
        
        if (filters != null && !filters.isEmpty()) {
            
            nextConnective += " ( ";
            
            for(String filterProperty : filters.keySet()) {
                
                if (filters.get(filterProperty) == null) {
                    continue;
                }
                
                switch (filterProperty) {
                
                case "nom":
                    query.append(nextConnective).append(" o.nom LIKE :nom");
                    queryParameters.put("nom", "%" + filters.get(filterProperty) + "%");
                    break;

                case "postNom":
                    query.append(nextConnective).append(" o.postNom LIKE :postNom");
                    queryParameters.put("postNom", "%" + filters.get(filterProperty) + "%");
                    break;

                case "prenom":
                    query.append(nextConnective).append(" o.prenom LIKE :prenom");
                    queryParameters.put("prenom", "%" + filters.get(filterProperty) + "%");
                    break;

                case "taille":
                    query.append(nextConnective).append(" o.taille = :taille");
                    queryParameters.put("taille", new BigDecimal(filters.get(filterProperty).toString()));
                    break;

                case "poids":
                    query.append(nextConnective).append(" o.poids = :poids");
                    queryParameters.put("poids", new BigDecimal(filters.get(filterProperty).toString()));
                    break;

                case "dateNaissance":
                    query.append(nextConnective).append(" o.dateNaissance = :dateNaissance");
                    queryParameters.put("dateNaissance", filters.get(filterProperty));
                    break;

                case "telephone":
                    query.append(nextConnective).append(" o.telephone LIKE :telephone");
                    queryParameters.put("telephone", "%" + filters.get(filterProperty) + "%");
                    break;

                case "numIdentification":
                    query.append(nextConnective).append(" o.numIdentification LIKE :numIdentification");
                    queryParameters.put("numIdentification", "%" + filters.get(filterProperty) + "%");
                    break;

                case "adresse":
                    query.append(nextConnective).append(" o.adresse LIKE :adresse");
                    queryParameters.put("adresse", "%" + filters.get(filterProperty) + "%");
                    break;

                }
                
                nextConnective = " AND";
            }
            
            query.append(" ) ");
            nextConnective = " AND";
        }
        
        if (sortField != null && !sortField.isEmpty()) {
            query.append(" ORDER BY o.").append(sortField);
            query.append(SortOrder.DESCENDING.equals(sortOrder) ? " DESC" : " ASC");
        }
        
        TypedQuery<CitoyenEntity> q = this.entityManager.createQuery(query.toString(), this.getType());
        
        for(String queryParameter : queryParameters.keySet()) {
            q.setParameter(queryParameter, queryParameters.get(queryParameter));
        }

        return q.setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
}
