package org.savanah.service;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Named;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.primefaces.model.SortOrder;
import org.savanah.domain.AntecedentJudiciaireEntity;
import org.savanah.service.security.SecurityWrapper;

@Named
public class AntecedentJudiciaireService extends BaseService<AntecedentJudiciaireEntity> implements Serializable {

    private static final long serialVersionUID = 1L;
    
    public AntecedentJudiciaireService(){
        super(AntecedentJudiciaireEntity.class);
    }
    
    @Transactional
    public List<AntecedentJudiciaireEntity> findAllAntecedentJudiciaireEntities() {
        
        return entityManager.createQuery("SELECT o FROM AntecedentJudiciaire o ", AntecedentJudiciaireEntity.class).getResultList();
    }
    
    @Override
    @Transactional
    public long countAllEntries() {
        return entityManager.createQuery("SELECT COUNT(o) FROM AntecedentJudiciaire o", Long.class).getSingleResult();
    }
    
    @Override
    @Transactional
    public AntecedentJudiciaireEntity save(AntecedentJudiciaireEntity antecedentJudiciaire) {
        String username = SecurityWrapper.getUsername();
        
        antecedentJudiciaire.updateAuditInformation(username);
        
        return super.save(antecedentJudiciaire);
    }
    
    @Override
    @Transactional
    public AntecedentJudiciaireEntity update(AntecedentJudiciaireEntity antecedentJudiciaire) {
        String username = SecurityWrapper.getUsername();
        antecedentJudiciaire.updateAuditInformation(username);
        return super.update(antecedentJudiciaire);
    }
    
    @Override
    protected void handleDependenciesBeforeDelete(AntecedentJudiciaireEntity antecedentJudiciaire) {

        /* This is called before a AntecedentJudiciaire is deleted. Place here all the
           steps to cut dependencies to other entities */
        
        this.cutAllAntecedentJudiciaireInfractionsAssignments(antecedentJudiciaire);
        
    }

    // Remove all assignments from all infraction a antecedentJudiciaire. Called before delete a antecedentJudiciaire.
    @Transactional
    private void cutAllAntecedentJudiciaireInfractionsAssignments(AntecedentJudiciaireEntity antecedentJudiciaire) {
        entityManager
                .createQuery("UPDATE Infraction c SET c.antecedentJudiciaire = NULL WHERE c.antecedentJudiciaire = :p")
                .setParameter("p", antecedentJudiciaire).executeUpdate();
    }
    
    // This is the central method called by the DataTable
    @Override
    @Transactional
    public List<AntecedentJudiciaireEntity> findEntriesPagedAndFilteredAndSorted(int firstResult, int maxResults, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        
        StringBuilder query = new StringBuilder();

        query.append("SELECT o FROM AntecedentJudiciaire o");
        
        String nextConnective = " WHERE";
        
        Map<String, Object> queryParameters = new HashMap<>();
        
        if (filters != null && !filters.isEmpty()) {
            
            nextConnective += " ( ";
            
            for(String filterProperty : filters.keySet()) {
                
                if (filters.get(filterProperty) == null) {
                    continue;
                }
                
                switch (filterProperty) {
                
                case "date":
                    query.append(nextConnective).append(" o.date = :date");
                    queryParameters.put("date", filters.get(filterProperty));
                    break;

                case "description":
                    query.append(nextConnective).append(" o.description LIKE :description");
                    queryParameters.put("description", "%" + filters.get(filterProperty) + "%");
                    break;

                }
                
                nextConnective = " AND";
            }
            
            query.append(" ) ");
            nextConnective = " AND";
        }
        
        if (sortField != null && !sortField.isEmpty()) {
            query.append(" ORDER BY o.").append(sortField);
            query.append(SortOrder.DESCENDING.equals(sortOrder) ? " DESC" : " ASC");
        }
        
        TypedQuery<AntecedentJudiciaireEntity> q = this.entityManager.createQuery(query.toString(), this.getType());
        
        for(String queryParameter : queryParameters.keySet()) {
            q.setParameter(queryParameter, queryParameters.get(queryParameter));
        }

        return q.setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
}
