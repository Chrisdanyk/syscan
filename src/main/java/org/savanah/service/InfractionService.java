package org.savanah.service;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Named;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.primefaces.model.SortOrder;
import org.savanah.domain.AntecedentJudiciaireEntity;
import org.savanah.domain.InfractionEntity;

@Named
public class InfractionService extends BaseService<InfractionEntity> implements Serializable {

    private static final long serialVersionUID = 1L;
    
    public InfractionService(){
        super(InfractionEntity.class);
    }
    
    @Transactional
    public List<InfractionEntity> findAllInfractionEntities() {
        
        return entityManager.createQuery("SELECT o FROM Infraction o ", InfractionEntity.class).getResultList();
    }
    
    @Override
    @Transactional
    public long countAllEntries() {
        return entityManager.createQuery("SELECT COUNT(o) FROM Infraction o", Long.class).getSingleResult();
    }
    
    @Override
    protected void handleDependenciesBeforeDelete(InfractionEntity infraction) {

        /* This is called before a Infraction is deleted. Place here all the
           steps to cut dependencies to other entities */
        
    }

    @Transactional
    public List<InfractionEntity> findAvailableInfractions(AntecedentJudiciaireEntity antecedentJudiciaire) {
        return entityManager.createQuery("SELECT o FROM Infraction o WHERE o.antecedentJudiciaire IS NULL", InfractionEntity.class).getResultList();
    }

    @Transactional
    public List<InfractionEntity> findInfractionsByAntecedentJudiciaire(AntecedentJudiciaireEntity antecedentJudiciaire) {
        return entityManager.createQuery("SELECT o FROM Infraction o WHERE o.antecedentJudiciaire = :antecedentJudiciaire", InfractionEntity.class).setParameter("antecedentJudiciaire", antecedentJudiciaire).getResultList();
    }

    // This is the central method called by the DataTable
    @Override
    @Transactional
    public List<InfractionEntity> findEntriesPagedAndFilteredAndSorted(int firstResult, int maxResults, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        
        StringBuilder query = new StringBuilder();

        query.append("SELECT o FROM Infraction o");
        
        // Can be optimized: We need this join only when antecedentJudiciaire filter is set
        query.append(" LEFT OUTER JOIN o.antecedentJudiciaire antecedentJudiciaire");
        
        String nextConnective = " WHERE";
        
        Map<String, Object> queryParameters = new HashMap<>();
        
        if (filters != null && !filters.isEmpty()) {
            
            nextConnective += " ( ";
            
            for(String filterProperty : filters.keySet()) {
                
                if (filters.get(filterProperty) == null) {
                    continue;
                }
                
                switch (filterProperty) {
                
                case "nom":
                    query.append(nextConnective).append(" o.nom LIKE :nom");
                    queryParameters.put("nom", "%" + filters.get(filterProperty) + "%");
                    break;

                case "peine":
                    query.append(nextConnective).append(" o.peine LIKE :peine");
                    queryParameters.put("peine", "%" + filters.get(filterProperty) + "%");
                    break;

                case "antecedentJudiciaire":
                    query.append(nextConnective).append(" o.antecedentJudiciaire = :antecedentJudiciaire");
                    queryParameters.put("antecedentJudiciaire", filters.get(filterProperty));
                    break;
                
                }
                
                nextConnective = " AND";
            }
            
            query.append(" ) ");
            nextConnective = " AND";
        }
        
        if (sortField != null && !sortField.isEmpty()) {
            query.append(" ORDER BY o.").append(sortField);
            query.append(SortOrder.DESCENDING.equals(sortOrder) ? " DESC" : " ASC");
        }
        
        TypedQuery<InfractionEntity> q = this.entityManager.createQuery(query.toString(), this.getType());
        
        for(String queryParameter : queryParameters.keySet()) {
            q.setParameter(queryParameter, queryParameters.get(queryParameter));
        }

        return q.setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
}
